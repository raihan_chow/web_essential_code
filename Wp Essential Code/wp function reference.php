/**********************************************************************************/

wp_get_attachment_url()
wp_get_attachment_image_src()
wp_get_attachment_thumb_url()

/**********************************************************************************/
____________________________________________________________________________________

Reference:

____________________________________________________________________________________

wp_get_attachment_url( 88, 'full' );
Get full image size from upload image id

wp_get_attachment_image_src( 88, 'full' );
Get full image size and other info like width and height from upload image id


wp_get_attachment_thumb_url();
Get Thumb image size from upload image id